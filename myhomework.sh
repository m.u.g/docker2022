docker network create mynet
docker run -d --name postgresContainer --network mynet -e POSTGRES_USER=root -e POSTGRES_PASSWORD=admin123 -v postgres_data:/var/lib/postgresql/data -v postgresql:/var/lib/postgresql postgres:latest
docker volume create --name sonarqube_data
docker volume create --name sonarqube_logs
docker volume create --name sonarqube_extensions
docker run -d --name sonarqubeContainer --network mynet -p 9000:9000 -e SONAR_JDBC_URL=jdbc:postgresql://postgresContainer/sonar -e SONAR_JDBC_USERNAME=root -e SONAR_JDBC_PASSWORD=admin123 -v sonarqube_data:/opt/sonarqube/data -v sonarqube_extensions:/opt/sonarqube/extensions -v sonarqube_logs:/opt/sonarqube/logs sonarqube:latest

docker run -d --name jenkinsContainer --network mynet -p 8080:8080 -p 50000:50000 -v jenkins_home:/var/jenkins_home  --restart=on-failure jenkins/jenkins:lts-jdk11

docker volume create --name nexus-data
docker run -d -p 8081:8081 --name nexusContainer --network mynet -v nexus-data:/nexus-data sonatype/nexus3

docker run -d -p 8000:8000 -p 9443:9443 --name portainerContainer --network mynet --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce:latest
